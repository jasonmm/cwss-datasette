(ns xml-to-csv
  "Functions to convert CWSS XML files to CSV files. See the comment just below
  this `ns` form for instructions."
  (:require [clojure.data.xml :as x]
            [clojure.data.csv :as csv]
            [clojure.string :as str]
            [clojure.java.io :as io]))

(comment
  ; Start a REPL.
  ; Load this file into the REPL.
  ; Eval the following `map` form to create the CSV files.
  (map xml->csv! ["data/battle-utf8.xml"
                  "data/battleunitlink-utf8.xml"
                  "data/persons-utf8.xml"
                  "data/units-utf8.xml"])


  #_())

(defn parse-xml
  "Slurp `filename` then call `clojure.data.xml/parse-str` on the contents."
  [^String filename]
  (let [xml-str (slurp filename)]
    (x/parse-str xml-str)))

(defn get-children
  "Given a collection of XML `elements` return the ones whose `:tag` equals `tag`."
  [tag elements]
  (filter #(= tag (:tag %)) elements))

(defn first-content
  "Return the first element's content."
  [elements]
  (-> elements first :content))

(defn first-child
  "Return the content of the first element whose tag is `tag`."
  [tag elements]
  (first-content (get-children tag elements)))

(defn uuid-str?
  "Is `s` a UUID string. UUID strings in this data set are always surrounded by {}."
  [s]
  (if (nil? s)
    s
    (re-find #"^\{[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}\}$"
             s)))

(defn property-value
  "'properties' is an XML tag used in the CWSS dataset that contains other tags
  which describe properties of the 'entry'. This function is given a property
  element and returns its value."
  [property]
  (let [null-attribute (get-in property [:attrs :m/null])
        value-is-null? (= "true" null-attribute)
        v              (when-not value-is-null?
                         (first (:content property)))]
    (if (uuid-str? v)
      (str/lower-case (str/replace v #"[\{\}]" "")) ; return just the UUID part
      v)))

(defn property->map
  "Given a child of the 'properties' tag return a map of one key (the property's
  tag) whose value is the property's value."
  [property]
  {(:tag property) (property-value property)})

(defn entry-properties
  "'entry' is an XML tag used in the CWSS dataset. This function converts an
  'entry' XML element into a map containing just the entry's properties."
  [entry]
  (->> entry
       :content
       (first-child :content)
       (first-child :properties)
       (map property->map)
       (apply merge)))

(defn row-map->row-vector
  "Convert a map containing column name to column value mappings into a vector
  suitable for use by `clojure.data.csv/write-csv`."
  [column-keys row-map]
  (map #(get row-map %) column-keys))

(defn write-csv-file
  "Write `csv-data` to the file named `csv-filename`."
  [csv-data csv-filename]
  (with-open [writer (io/writer csv-filename)]
    (csv/write-csv writer csv-data)))

(defn xml->csv
  "Return data suitable for use by `clojure.data.csv/write-csv` from the given
  XML filename."
  [input-filename]
  (let [xml         (parse-xml input-filename)
        row-maps    (->> xml
                         :content
                         (get-children :entry)
                         (map entry-properties))
        column-keys (keys (first row-maps))
        csv-header  (mapv name column-keys)
        csv-rows    (mapv (partial row-map->row-vector column-keys) row-maps)]
    (concat [csv-header] csv-rows)))

(defn xml->csv!
  "Creates a CSV file from the given CWSS XML file."
  ([input-filename]
   (let [csv-filename (-> input-filename
                          (str/split #"\.")
                          first
                          (str ".csv"))]
     (xml->csv! input-filename csv-filename)))
  ([input-filename output-filename]
   (print input-filename " -> " output-filename)
   (let [csv-data (xml->csv input-filename)]
     (println " .." (count csv-data) "rows")
     (write-csv-file csv-data output-filename))))

(comment

  ;; scratchpad below this line

  (xml->csv! "data/battle-fixed.xml")

  (def xml (parse-xml "data2/battle-new.xml"))
  (type xml)
  (nth xml 2)

  (def row-maps
    (->> xml
         :content
         (get-children :entry)
         ;(take 2)
         (map entry-properties)))

  (def column-keys (keys (first row-maps)))

  (def csv-header (map name column-keys))

  (def csv-rows (map (partial row-map->row-vector column-keys) row-maps))

  (take 3 csv-rows)

  (write-csv-file csv-rows "data2/battle-new.csv")


  ; number of <entry> tags in the file.
  (count (->> xml :content (get-children :entry)))

  (uuid-str? "{770548CC-5346-41FA-98AC-1A2929E398A1}")
  (uuid-str? "Operations")
  (uuid-str? nil)

  #_())