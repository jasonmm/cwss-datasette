# Description

Convert the raw CWSS data into a [Datasette](https://datasette.io/) database.

# XML File Encoding Conversion

The [raw CWSS data](https://www.nps.gov/featurecontent/foia/CWSS.zip) ([source](https://fogandfriction.com/2017/07/07/the-civil-war-soldiers-and-sailors-database/)) appears to be an export from an SQL Server database. The XML files have encodings that seem to be CP1252 which causes problems for the Clojure XML parser. Using _iconv_ converts the encoding into UTF-8 (as specified by the XML header) which can then be read by the Clojure XML parser. For example:

```shell
iconv -f CP1252 -t UTF-8 battle.xml > battle-utf8.xml
```

After unzipping _CWSS.zip_ into a separate directory, run the following command to convert all XML files to UTF-8 encoding.

```shell
for i in *.xml; do \
  iconv -f CP1252 -t UTF-8 "$i" > "$(basename -s .xml "$i")-utf8.xml"; \
done
```

# XML To CSV Conversion

Open the _src/xml-to-csv.clj_ file and follow the instructions in the `comment` at the top of the file.

Then rename the CSV files to something nice. This is important because _Datasette_ uses the filenames to name the database tables.

```shell
mv battle-utf8.csv battle.csv
mv battleunitlink-utf8.csv battleunitlink.csv
mv persons-utf8.csv persons.csv
mv units-utf8.csv units.csv
```

# Create The Datasette Database

```shell
mkdir datasette
cd datasette
csvs-to-sqlite ../data/battle.csv ../data/battleunitlink.csv ../data/persons.csv ../data/units.csv cwss.db
```

# Running Locally

```shell
datasette cwss.db -o
```

# Deploying

```shell
datasette publish cloudrun --service=cwss --metadata=metadata.yml --name=cwss cwss.db
```

# Using The SQL Server Backup File

The _CWSS.zip_ file contains a SQL Server backup file that contains SQL Server database, presumably for the CWSS data. It could be easy to create a SQLite database from this. However, getting an SQL Server running on a Mac with sufficient space to restore the backup has proved to not be straight forward.

## Resources

* https://docs.microsoft.com/en-us/sql/linux/quickstart-install-connect-docker?view=sql-server-ver15&preserve-view=true&pivots=cs1-bash
* https://docs.microsoft.com/en-us/sql/linux/tutorial-restore-backup-in-sql-server-container?view=sql-server-ver15
* https://docs.microsoft.com/en-us/sql/linux/sql-server-linux-docker-container-configure?view=sql-server-ver15&pivots=cs1-bash

## Command Lines

##### Create container
```shell
$ sudo docker run -e "ACCEPT_EULA=Y" -e "SA_PASSWORD=___" -p 1433:1433 --name sql1 -h sql1 -d mcr.microsoft.com/mssql/server:2019-latest
```

##### Make directory
```shell
$ sudo docker exec -it sql1 mkdir /var/opt/mssql/backup
```

##### Copy the backup file into container
```shell
$ sudo docker cp data/cwss/data/nps_cwss-20121031.bak sql1:/var/opt/mssql/backup
```

##### Inspect backup file
```shell
$ sudo docker exec -it sql1 /opt/mssql-tools/bin/sqlcmd -S localhost -U SA -P '___' -Q 'RESTORE FILELISTONLY FROM DISK = "/var/opt/mssql/backup/nps_cwss-20121031.bak"' | tr -s ' ' | cut -d ' ' -f 1-2
```

##### Perform restore
```shell
$ sudo docker exec -it sql1 /opt/mssql-tools/bin/sqlcmd  -S localhost -U SA -P '___' -Q 'RESTORE DATABASE nps_cwss FROM DISK = "/var/opt/mssql/backup/nps_cwss-20121031.bak" WITH MOVE "testdb" TO "/var/opt/mssql/data/nps_cwss_data.mdf", MOVE "testdb_log" TO "/var/opt/mssql/data/nps_cwss_log.ldf"'
```

